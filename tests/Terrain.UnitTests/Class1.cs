﻿using System;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using NUnit.Framework;
using Terrain.App;
using Terrain.App.Map;
using Terrain.App.Types;

namespace Terrain.UnitTests
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void Test()
        {
            const int mapSize = 64;

            var path = Path.Combine(Directory.GetCurrentDirectory(), "islands.png");
            var islands = NoiseGenerator.GenerateNoise(new IndexSize(mapSize, mapSize), new MapOptions());
            var graphicsDevice = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, new PresentationParameters());
            islands.SaveToFile(graphicsDevice, path);

            Console.WriteLine(path);
        }
    }

    [TestFixture]
    public class Class2
    {
        [Test]
        public void Test()
        {
            const int mapSize = 64;

            var path = Path.Combine(Directory.GetCurrentDirectory(), "islands.png");
            var islands = NoiseGenerator.GenerateNoise2(new IndexSize(mapSize, mapSize), new NoiseOptions());
            var graphicsDevice = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, new PresentationParameters());
            islands.SaveToFile(graphicsDevice, path);

            Console.WriteLine(path);
        }
    }
}