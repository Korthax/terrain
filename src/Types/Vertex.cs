using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Terrain.App.Types
{
    public struct Vertex
    {
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Color, VertexElementUsage.Color, 0),
            new VertexElement(16, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(24, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)
        );

        public Vector3 Position { get; }
        public Color Colour { get; }
        public Vector2 Texture { get; }
        public Vector2 Edges { get; }

        public static Vertex Transform(Vertex vertex, Matrix matrix)
        {
            return new Vertex(Vector3.Transform(vertex.Position, matrix), vertex.Texture, vertex.Edges, vertex.Colour);
        }

        public Vertex(Vector3 position, Vector2 texture, Vector2 edges, Color colour)
            : this()
        {
            Position = position;
            Texture = texture;
            Edges = edges;
            Colour = colour;
        }

        public bool Equals(Vertex other)
        {
            return Position.Equals(other.Position) && Colour.Equals(other.Colour) && Texture.Equals(other.Texture) && Edges.Equals(other.Edges);
        }

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(null, obj))
                return false;

            return obj is Vertex vertex && Equals(vertex);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Position.GetHashCode();
                hashCode = (hashCode * 397) ^ Colour.GetHashCode();
                hashCode = (hashCode * 397) ^ Texture.GetHashCode();
                hashCode = (hashCode * 397) ^ Edges.GetHashCode();
                return hashCode;
            }
        }
    }
}