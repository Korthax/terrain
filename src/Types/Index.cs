using System;
using Microsoft.Xna.Framework;

namespace Terrain.App.Types
{
    public class Index
    {
        public static readonly Index[] Neighbours =
        {
            new Index(0, -1),
            new Index(-1, 0),
            new Index(1, 0),
            new Index(0, 1)
        };

        public static readonly Index[] AllNeighbours =
        {
            new Index(-1, -1),
            new Index(1, -1),
            new Index(1, 1),
            new Index(-1, 1),
            new Index(0, -1),
            new Index(-1, 0),
            new Index(1, 0),
            new Index(0, 1),
            new Index(1, 1),
        };

        public int X { get; }
        public int Y { get; }

        public static Index From(Vector2 vector)
        {
            return new Index((int)vector.X, (int)vector.Y);
        }

        public static Index From(double x, double y, Position centre)
        {
            var dx = x > centre.X ? Math.Ceiling(x) : Math.Floor(x);
            var dy = x > centre.Y ? Math.Ceiling(y) : Math.Floor(y);
            return new Index((int)dx, (int)dy);
        }

        public Index(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(Index a, Index b)
        {
            if (ReferenceEquals(null, a) && ReferenceEquals(null, b))
                return true;

            if (ReferenceEquals(null, a) || ReferenceEquals(null, b))
                return false;

            return a.Equals(b);
        }

        public static Index operator +(Index a, Index b)
        {
            return new Index(a.X + b.X, a.Y + b.Y);
        }

        public static bool operator !=(Index a, Index b)
        {
            return !(a == b);
        }

        protected bool Equals(Index other)
        {
            return Y == other.Y && X == other.X;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((Index)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Y * 397) ^ X;
            }
        }
    }
}