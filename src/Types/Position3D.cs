using Microsoft.Xna.Framework;

namespace Terrain.App.Types
{
    public sealed class Position3D
    {
        public static Position3D Zero => new Position3D(0, 0, 0);

        public float X { get; }
        public float Y { get; }
        public float Z { get; }

        public Position3D(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Position3D operator +(Position3D a, Size3D b)
        {
            return new Position3D(a.X + b.Width, a.Y + b.Height, a.Z + b.Depth);
        }

        public static Position3D operator -(Position3D a, Size3D b)
        {
            return new Position3D(a.X - b.Width, a.Y - b.Height, a.Z - b.Depth);
        }

        public override string ToString()
        {
            return $"{{{X},{Y},{Z}}}";
        }

        public Vector3 ToVector3()
        {
            return new Vector3(X, Y, Z);
        }
    }
}