﻿using Microsoft.Xna.Framework;

namespace Terrain.App.Types
{
    public struct Matrix2X2
    {
        public static Matrix2X2 Identity => new Matrix2X2(1, 0, 1, 0);

        public float M11 { get; }
        public float M12 { get; }
        public float M21 { get; }
        public float M22 { get; }

        public Matrix2X2(float m11, float m12, float m21, float m22)
        {
            M11 = m11;
            M12 = m12;
            M21 = m21;
            M22 = m22;
        }

        public static Matrix2X2 Add(Matrix2X2 a, Matrix2X2 b)
        {
            return new Matrix2X2(a.M11 + b.M11, a.M12 + b.M12, a.M21 + b.M21, a.M22 + b.M22);
        }

        public static Matrix2X2 CreateScale(float scale)
        {
            return new Matrix2X2(scale, 0, 0, scale);
        }

        public static Matrix2X2 Invert(Matrix2X2 matrix)
        {
            var determinantInverse = 1 / (matrix.M11 * matrix.M22 - matrix.M12 * matrix.M21);
            var m11 = matrix.M22 * determinantInverse;
            var m12 = -matrix.M12 * determinantInverse;

            var m21 = -matrix.M21 * determinantInverse;
            var m22 = matrix.M11 * determinantInverse;

            return new Matrix2X2(m11, m12, m21, m22);
        }

        public static Matrix2X2 Multiply(Matrix2X2 a, Matrix2X2 b)
        {
            var resultM11 = a.M11 * b.M11 + a.M12 * b.M21;
            var resultM12 = a.M11 * b.M12 + a.M12 * b.M22;

            var resultM21 = a.M21 * b.M11 + a.M22 * b.M21;
            var resultM22 = a.M21 * b.M12 + a.M22 * b.M22;

            return new Matrix2X2(resultM11, resultM12, resultM21, resultM22);
        }

        public static Matrix2X2 Negate(Matrix2X2 matrix)
        {
            return new Matrix2X2(-matrix.M11, -matrix.M12, -matrix.M21, -matrix.M22);
        }

        public static Matrix2X2 Subtract(Matrix2X2 a, Matrix2X2 b)
        {
            return new Matrix2X2(a.M11 - b.M11, a.M12 - b.M12, a.M21 - b.M21, a.M22 - b.M22);
        }

        public static Vector Transform(Vector2 v, Matrix2X2 matrix)
        {
            return new Vector(v.X * matrix.M11 + v.Y * matrix.M21, v.X * matrix.M12 + v.Y * matrix.M22);
        }

        public static Matrix2X2 Transpose(Matrix2X2 matrix)
        {
            return new Matrix2X2(matrix.M11, matrix.M21, matrix.M12, matrix.M22);
        }

        public float Determinant()
        {
            return M11 * M22 - M12 * M21;
        }

        public override string ToString()
        {
            return $"{{{M11}, {M12}}} {{{M21}, {M22}}}";
        }
    }
}