﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Terrain.App.Types
{
    public static class Colour
    {
        private static readonly List<Color> Colours = new List<Color>
        {
            new Color(178, 0, 0), new Color(242, 194, 0), new Color(163, 217, 177),
            new Color(0, 204, 255), new Color(0, 0, 102), new Color(251, 191, 255),
            new Color(102, 51, 51), new Color(204, 194, 153), new Color(0, 191, 77),
            new Color(0, 119, 179), new Color(94, 86, 115), new Color(242, 61, 157),
            new Color(255, 208, 191), new Color(238, 255, 0), new Color(57, 115, 80),
            new Color(19,57,77), new Color(162,0,242), new Color(128,64,98),
            new Color(64,26,0), new Color(61,64,16), new Color(61,242,206),
            new Color(143,169,191), new Color(34,0,51), new Color(140,0,56),
            new Color(191,105,48), new Color(185,191,96), new Color(16,64,61),
            new Color(0,56,140), new Color(119,35,140), new Color(51,0,20),
            new Color(115,75,29), new Color(86,115,29), new Color(0,155,166),
            new Color(128,145,255), new Color(48,38,51), new Color(204,153,167),
            new Color(217,166,108), new Color(0,255,34), new Color(182,238,242),
            new Color(0,0,166), new Color(234,121,242), new Color(242,61,85)
        };

        public static Color For(int value)
        {
            while (value > Colours.Count - 1)
                value -= Colours.Count;

            return Colours[value];
        }
    }
}