using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Map;

namespace Terrain.App.Types
{
    [Serializable]
    public struct InstanceData : IVertexType
    {
        public VertexDeclaration VertexDeclaration => new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 0),
            new VertexElement(16, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 1),
            new VertexElement(32, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 2),
            new VertexElement(48, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 3),
            new VertexElement(64, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 2),
            new VertexElement(72, VertexElementFormat.Single, VertexElementUsage.TextureCoordinate, 3)
        );

        public Matrix World { get; }
        public Vector2 AtlasCoordinate { get; }
        public ShaderFlag Flags { get; private set; }

        public InstanceData(Matrix world, Vector2 atlasCoordinate, ShaderFlag flags)
        {
            World = world;
            AtlasCoordinate = atlasCoordinate;
            Flags = flags;
        }

        public void SetFlag(ShaderFlag shaderFlag)
        {
            Flags |= shaderFlag;
        }
    }
}