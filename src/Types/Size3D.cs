using System;

namespace Terrain.App.Types
{
    public class Size3D
    {
        public static Size3D Zero => new Size3D(0, 0, 0);

        public float Volume => Width * Height * Depth;
        public float Width { get; }
        public float Height { get; }
        public float Depth { get; }

        public Size3D(float width, float height, float depth)
        {
            Width = width;
            Height = height;
            Depth = depth;
        }

        public float Max()
        {
            return Width > Height ? Width : Height;
        }

        public static Size3D operator *(Size3D size, float scale)
        {
            return new Size3D(size.Width * scale, size.Height * scale, size.Depth * scale);
        }

        public static Size3D operator /(Size3D size, float scale)
        {
            return new Size3D(size.Width / scale, size.Height / scale, size.Depth / scale);
        }

        public static Size3D operator *(Size3D size, Vector scale)
        {
            return new Size3D(size.Width * scale.X, size.Height, size.Depth * scale.Y);
        }

        public static bool operator ==(Size3D a, Size3D b)
        {
            if(ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return Math.Abs(a.Width - b.Width) < float.Epsilon 
                   && Math.Abs(a.Height - b.Height) < float.Epsilon 
                   && Math.Abs(a.Depth - b.Depth) < float.Epsilon;
        }

        public static bool operator !=(Size3D a, Size3D b)
        {
            return !(a == b);
        }

        public bool Equals(Size3D other)
        {
            return Width.Equals(other.Width) && Height.Equals(other.Height) && Depth.Equals(other.Depth);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            return obj is Size size && Equals(size);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Width.GetHashCode();
                hashCode = (hashCode * 397) ^ Height.GetHashCode();
                hashCode = (hashCode * 397) ^ Depth.GetHashCode();
                return hashCode;
            }
        }
    }
}