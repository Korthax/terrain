namespace Terrain.App.Types
{
    public class IndexSize
    {
        public static IndexSize Zero => new IndexSize(0, 0); 
        public int Width { get; set; }
        public int Height { get; set; }

        public IndexSize(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public bool Contains(Index index)
        {
            return 0 <= index.X && 0 <= index.Y && index.X < Width && index.Y < Height;
        }
    }
}