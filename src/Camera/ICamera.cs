using Microsoft.Xna.Framework;
using Terrain.App.Input;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Camera
{
    public interface ICamera
    {
        void Update(GameTime gameTime, InputDetector inputDetector);
        void SetOn(IEffectShader effect);
        Vector3 Unproject(Vector3 vector);
        void Set(ICameraSubject cameraSubject);
        void BindTo(InputDetector inputDetector);
        bool IsVisible(BoundingBox boundingBox);
    }
}