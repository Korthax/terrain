using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Input;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Camera
{
    public class ChaseCamera : ICamera
    {
        private readonly GraphicsDevice _graphicsDevice;
        private BoundingFrustum _boundingFrustum;
        private ICameraSubject _cameraSubject;
        private Matrix _projectionMatrix;
        private Matrix _viewMatrix;

        private readonly Vector3 _desiredPositionOffset = new Vector3(0.0f, 10.00f, 20.50f);
        private readonly Vector3 _lookAtOffset = new Vector3(0.0f, 0.1500f, 0.0f);

        private Vector3 _position;

        public void Update(GameTime gameTime, InputDetector inputDetector)
        {
            // Construct a matrix to transform from object space to worldspace
            var transform = Matrix.Identity;
            transform.Forward = _cameraSubject.Direction;
            transform.Up = Vector3.Up;
            transform.Right = Vector3.Cross(Vector3.Up, _cameraSubject.Direction);

            // Calculate desired camera properties in world space
            _position = _cameraSubject.Position + Vector3.TransformNormal(_desiredPositionOffset, transform);
            var lookAt = _cameraSubject.Position + Vector3.TransformNormal(_lookAtOffset, transform);

            _viewMatrix = Matrix.CreateLookAt(_position, lookAt, Vector3.Up);
            _projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45.0f), _graphicsDevice.Viewport.AspectRatio, 1.0f, 100000.0f);
            _boundingFrustum = new BoundingFrustum(_viewMatrix * _projectionMatrix);
        }

        public ChaseCamera(ICameraSubject cameraSubject, GraphicsDevice graphicsDevice)
        {
            _cameraSubject = cameraSubject;
            _graphicsDevice = graphicsDevice;
        }

        public void Set(ICameraSubject cameraSubject)
        {
            _cameraSubject = cameraSubject;
        }

        public void BindTo(InputDetector inputDetector)
        {
        }

        public bool IsVisible(BoundingBox boundingBox)
        {
            return _boundingFrustum.Intersects(boundingBox);
        }

        public void SetOn(IEffectShader effect)
        {
            effect.SetCamera(_viewMatrix, _projectionMatrix);
        }

        public Vector3 Unproject(Vector3 screenPosition)
        {
            return _graphicsDevice.Viewport.Unproject(screenPosition, _projectionMatrix, _viewMatrix, Matrix.Identity);
        }

        public class NoSubject : ICameraSubject
        {
            public Vector3 Position => Vector3.Zero;
            public Vector3 Direction => Vector3.Zero;
        }
    }
}