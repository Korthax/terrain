using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Terrain.App.Input;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Camera
{
    public class CompositeCamera : ICamera
    {
        private readonly ICamera[] _cameras;
        private int _current;

        public CompositeCamera(params ICamera[] cameras)
        {
            _cameras = cameras;
        }

        public void Update(GameTime gameTime, InputDetector inputDetector)
        {
            _cameras[_current].Update(gameTime, inputDetector);
        }

        public void SetOn(IEffectShader effect)
        {
            _cameras[_current].SetOn(effect);
        }

        public Vector3 Unproject(Vector3 vector)
        {
            return _cameras[_current].Unproject(vector);
        }

        public bool IsVisible(BoundingBox boundingBox)
        {
            return _cameras[_current].IsVisible(boundingBox);
        }

        public void Set(ICameraSubject cameraSubject)
        {
            _cameras[_current].Set(cameraSubject);
        }

        public void BindTo(InputDetector inputDetector)
        {
            inputDetector.OnKeysPressed.Subscribe(input =>
            {
                if(input.KeySet.Contains(Keys.D1))
                    _current = _current == _cameras.Length - 1 ? 0 : _current + 1;
            });

            foreach (var camera in _cameras)
                camera.BindTo(inputDetector);
        }
    }
}