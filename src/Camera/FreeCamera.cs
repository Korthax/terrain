using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Terrain.App.Input;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Camera
{
    public class FreeCamera : ICamera
    {
        private static readonly Vector3 Forward = Vector3.UnitZ;
        private const float TurnSpeed = 0.1f;
        private const float MoveSpeed = 0.01f;

        private readonly GraphicsDevice _graphicsDevice;
        private readonly Matrix _projectionMatrix;

        private BoundingFrustum _boundingFrustum;
        private Vector2 _mouseRotationBuffer;
        private Vector3 _cameraPosition;
        private Vector3 _cameraRotation;
        private Vector3 _moveVector;
        private Matrix _viewMatrix;
        private Vector3 _target;

        public FreeCamera(Vector3 position, GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
            _cameraPosition = position;
            _projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, _graphicsDevice.Viewport.AspectRatio, 1f, 2000f);
            _mouseRotationBuffer = Vector2.Zero;
            _target = Forward;
            _moveVector = Vector3.Zero;
            _boundingFrustum = new BoundingFrustum(_viewMatrix * _projectionMatrix);
        }

        public void BindTo(InputDetector inputDetector)
        {
            inputDetector.OnKeysDown.Subscribe(inputEvent =>
            {
                if(inputEvent.KeySet.Contains(Keys.S))
                    _moveVector.Z = -1;

                if (inputEvent.KeySet.Contains(Keys.W))
                    _moveVector.Z = 1;

                if (inputEvent.KeySet.Contains(Keys.A))
                    _moveVector.X = 1;

                if (inputEvent.KeySet.Contains(Keys.D))
                    _moveVector.X = -1;

                if (inputEvent.KeySet.Contains(Keys.E))
                    _moveVector.Y += -1;

                if (inputEvent.KeySet.Contains(Keys.Q))
                    _moveVector.Y += 1;
            });
        }

        public void Update(GameTime gameTime, InputDetector inputDetector)
        {
            var elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            var currentMouseState = inputDetector.MousePosition();

            var moveVector = Vector3.Zero;
            var turnAmount = TurnSpeed * elapsed;
            var moveAmount = MoveSpeed * elapsed;

            moveVector.Z = _moveVector.Z * moveAmount;
            moveVector.X = _moveVector.X * moveAmount;
            moveVector.Y = _moveVector.Y * moveAmount;

            if (moveVector != Vector3.Zero)
                _cameraPosition += Vector3.Transform(Vector3.Normalize(moveVector), Matrix.CreateRotationY(_cameraRotation.Y));

            _mouseRotationBuffer.X -= turnAmount * (currentMouseState.X - _graphicsDevice.Viewport.Width / 2f);
            _mouseRotationBuffer.Y -= turnAmount * (currentMouseState.Y - _graphicsDevice.Viewport.Height / 2f);

            _mouseRotationBuffer.Y = MathHelper.Clamp(_mouseRotationBuffer.Y, MathHelper.ToRadians(-89), MathHelper.ToRadians(89));

            _cameraRotation = new Vector3(-_mouseRotationBuffer.Y, MathHelper.WrapAngle(_mouseRotationBuffer.X), 0);
            _target = Vector3.Transform(Forward, Matrix.CreateRotationX(_cameraRotation.X) * Matrix.CreateRotationY(_cameraRotation.Y));

            _viewMatrix = Matrix.CreateLookAt(_cameraPosition, _cameraPosition + _target, Vector3.Up);

            inputDetector.SetMousePosition(_graphicsDevice.Viewport.Width / 2, _graphicsDevice.Viewport.Height / 2);
            _moveVector = Vector3.Zero;
            _boundingFrustum = new BoundingFrustum(_viewMatrix * _projectionMatrix);
        }

        public void SetOn(IEffectShader effect)
        {
            effect.SetCamera(_viewMatrix, _projectionMatrix);
        }

        public bool IsVisible(BoundingBox boundingBox)
        {
            return _boundingFrustum.Intersects(boundingBox);
        }

        public Vector3 Unproject(Vector3 screenPosition)
        {
            return _graphicsDevice.Viewport.Unproject(screenPosition, _projectionMatrix, _viewMatrix, Matrix.Identity);
        }

        public void Set(ICameraSubject cameraSubject)
        {
            _cameraPosition = cameraSubject.Position;
        }
    }
}