using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Terrain.App.Input;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Camera
{
    public class StaticCamera : ICamera
    {
        private readonly GraphicsDevice _graphicsDevice;
        private readonly Matrix _projectionMatrix;
        private BoundingFrustum _boundingFrustum;
        private Matrix _viewMatrix;
        private Vector3 _position;

        public StaticCamera(Vector3 position, GraphicsDevice graphicsDevice)
        {
            _position = position;
            _graphicsDevice = graphicsDevice;
            _projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, graphicsDevice.Viewport.AspectRatio, 1f, 2000f);
            _boundingFrustum = new BoundingFrustum(_viewMatrix * _projectionMatrix);
        }

        public void Set(ICameraSubject cameraSubject)
        {
        }

        public void BindTo(InputDetector inputDetector)
        {
            inputDetector.OnKeysDown.Subscribe(inputEvent =>
            {
                if (inputEvent.KeySet.Contains(Keys.S))
                    _position.Z += 1;

                if (inputEvent.KeySet.Contains(Keys.W))
                    _position.Z += -1;

                if (inputEvent.KeySet.Contains(Keys.A))
                    _position.X += -1;

                if (inputEvent.KeySet.Contains(Keys.D))
                    _position.X += 1;

                if (inputEvent.KeySet.Contains(Keys.E))
                    _position.Y += 1;

                if (inputEvent.KeySet.Contains(Keys.Q) && _position.Y > 1)
                    _position.Y += -1;
            });
        }

        public void Update(GameTime gameTime, InputDetector inputDetector)
        {
            _viewMatrix = Matrix.CreateLookAt(_position, new Vector3(_position.X, 0, _position.Z), new Vector3(0, 0, -1));
            _boundingFrustum = new BoundingFrustum(_viewMatrix * _projectionMatrix);
        }

        public bool IsVisible(BoundingBox boundingBox)
        {
            return _boundingFrustum.Intersects(boundingBox);
        }

        public void SetOn(IEffectShader effect)
        {
            effect.SetCamera(_viewMatrix, _projectionMatrix);
        }

        public Vector3 Unproject(Vector3 vector)
        {
            return _graphicsDevice.Viewport.Unproject(vector, _projectionMatrix, _viewMatrix, Matrix.Identity);
        }
    }
}