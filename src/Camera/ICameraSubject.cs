using Microsoft.Xna.Framework;

namespace Terrain.App.Camera
{
    public interface ICameraSubject
    {
        Vector3 Position { get; }
        Vector3 Direction { get; }
    }
}