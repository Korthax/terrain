using System;

namespace Terrain.App.Map
{
    [Flags]
    public enum ShaderFlag
    {
        None = 0,
        Highlighted = 1
    }
}