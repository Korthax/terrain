﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Camera;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Rendering
{
    public class Renderer
    {
        private readonly RasterizerState _graphicsDeviceRasterizerState;
        private readonly GraphicsDevice _graphicsDevice;
        private readonly EffectShader _effect;
        private readonly ICamera _camera;

        public Renderer(EffectShader effect, GraphicsDevice graphicsDevice, ICamera camera)
        {
            _effect = effect;
            _graphicsDevice = graphicsDevice;
            _camera = camera;
            _graphicsDeviceRasterizerState = new RasterizerState
            {
                CullMode = CullMode.CullCounterClockwiseFace,
                DepthClipEnable = true,
                FillMode = FillMode.Solid
            };
        }

        public void DrawFrom(IBuffer buffer, Matrix position, FillMode fillMode = FillMode.WireFrame)
        {
            _graphicsDevice.RasterizerState = _graphicsDeviceRasterizerState;

            buffer.SetOn(_graphicsDevice, _camera);
            _camera.SetOn(_effect);
            _effect.ApplyTo(buffer, _graphicsDevice);
        }
    }
}