using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Camera;
using Terrain.App.Rendering.Objects;
using Terrain.App.Types;

namespace Terrain.App.Rendering.Buffers
{
    public class InstancedBuffer : IBuffer
    {
        private readonly List<RenderObject> _displayObjects;
        private readonly VertexBufferBinding[] _bindings;
        private readonly IndexBuffer _indexBuffer;

        private DynamicVertexBuffer _instanceBuffer;

        public static InstancedBuffer From(GraphicsDevice graphicsDevice, RenderObject instance)
        {
            var indices = new List<short>();
            var indexLookup = new Dictionary<Vertex, short>();
            var vertices = new List<Vertex>();

            foreach (var vertex in instance.Vertices)
            {
                if (!indexLookup.ContainsKey(vertex))
                {
                    indexLookup.Add(vertex, (short)vertices.Count);
                    vertices.Add(vertex);
                }

                indices.Add(indexLookup[vertex]);
            }

            var vertexBuffer = new VertexBuffer(graphicsDevice, Vertex.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);
            vertexBuffer.SetData(vertices.ToArray());

            var indexBuffer = new IndexBuffer(graphicsDevice, typeof(short), indices.Count, BufferUsage.WriteOnly);
            indexBuffer.SetData(indices.ToArray());

            var bindings = new VertexBufferBinding[2];
            bindings[0] = new VertexBufferBinding(vertexBuffer);

            return new InstancedBuffer(bindings, indexBuffer, new List<RenderObject>());
        }

        private InstancedBuffer(VertexBufferBinding[] bindings, IndexBuffer indexBuffer, List<RenderObject> displayObjects)
        {
            _bindings = bindings;
            _indexBuffer = indexBuffer;
            _displayObjects = displayObjects;
        }

        public void Dispose()
        {
            foreach(var binding in _bindings)
                binding.VertexBuffer.Dispose();

            _indexBuffer.Dispose();
        }

        public bool TryAdd(RenderObject renderObject)
        {
            _displayObjects.Add(renderObject);
            return true;
        }

        public void SetOn(GraphicsDevice graphicsDevice, ICamera camera)
        {
            var instances = _displayObjects
                .Where(x => x.IsVisible(camera))
                .Select(x => x.InstanceData)
                .ToArray();

            _instanceBuffer = null;
            if (instances.Length == 0)
                return;

            _instanceBuffer = new DynamicVertexBuffer(graphicsDevice, typeof(InstanceData), instances.Length, BufferUsage.WriteOnly);
            _instanceBuffer.SetData(instances);

            _bindings[1] = new VertexBufferBinding(_instanceBuffer, 0, 1);

            graphicsDevice.SetVertexBuffers(_bindings);
            graphicsDevice.Indices = _indexBuffer;
        }

        public void DrawTo(GraphicsDevice graphicsDevice)
        {
            if (_instanceBuffer == null || _instanceBuffer.VertexCount == 0)
                return;

            graphicsDevice.DrawInstancedPrimitives(PrimitiveType.TriangleList, 0, 0, _indexBuffer.IndexCount / 3, _instanceBuffer.VertexCount);
        }
    }
}