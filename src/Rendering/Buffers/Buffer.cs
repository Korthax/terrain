using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Camera;
using Terrain.App.Rendering.Objects;
using Terrain.App.Types;

namespace Terrain.App.Rendering
{
    public interface IBuffer
    {
        void Dispose();
        bool TryAdd(RenderObject model);
        void SetOn(GraphicsDevice graphicsDevice, ICamera camera);
        void DrawTo(GraphicsDevice graphicsDevice);
    }

    public class Buffer : IBuffer
    {
        private readonly Dictionary<Vertex, short> _indexLookup;
        private readonly HashSet<RenderObject> _displayObjects;
        private readonly List<Vertex> _vertices;
        private readonly List<short> _indices;

        private VertexBuffer _vertexBuffer;
        private IndexBuffer _indexBuffer;
        private bool _isDirty;

        public static Buffer From(GraphicsDevice graphicsDevice)
        {
            var vertexBuffer = new VertexBuffer(graphicsDevice, Vertex.VertexDeclaration, 1, BufferUsage.WriteOnly);
            var indexBuffer = new IndexBuffer(graphicsDevice, typeof(short), 1, BufferUsage.WriteOnly);
            return new Buffer(vertexBuffer, indexBuffer, false, new Dictionary<Vertex, short>(), new HashSet<RenderObject>(), new List<short>(), new List<Vertex>());
        }

        private Buffer(VertexBuffer vertexBuffer, IndexBuffer indexBuffer, bool isDirty, Dictionary<Vertex, short> indexLookup, HashSet<RenderObject> displayObjects, List<short> indices, List<Vertex> vertices)
        {
            _vertexBuffer = vertexBuffer;
            _indexBuffer = indexBuffer;
            _isDirty = isDirty;
            _indexLookup = indexLookup;
            _displayObjects = displayObjects;
            _indices = indices;
            _vertices = vertices;
        }

        public void Dispose()
        {
            _vertexBuffer.Dispose();
            _indexBuffer.Dispose();
        }

        public bool TryAdd(RenderObject model)
        {
            _displayObjects.Add(model);
            _isDirty = true;
            return true;
        }

        public void Process(RenderObject model, ICamera camera)
        {
            if(!model.IsVisible(camera))
                return;

            foreach (var vertex in model.Vertices)
            {
                if(!_indexLookup.ContainsKey(vertex))
                {
                    _indexLookup.Add(vertex, (short)_vertices.Count);
                    _vertices.Add(vertex);
                }

                _indices.Add(_indexLookup[vertex]);
            }
        }

        public bool TryRemove(RenderObject model)
        {
            _displayObjects.Remove(model);
            return true;
        }

        public void SetOn(GraphicsDevice graphicsDevice, ICamera camera)
        {
            //if (_isDirty)
            {
                _vertices.Clear();
                _indices.Clear();
                _indexLookup.Clear();

                foreach (var model in _displayObjects)
                    Process(model, camera);

                if(_vertices.Count == 0)
                    return;

                _vertexBuffer = new VertexBuffer(graphicsDevice, Vertex.VertexDeclaration, _vertices.Count, BufferUsage.WriteOnly);
                _vertexBuffer.SetData(_vertices.ToArray());

                _indexBuffer = new IndexBuffer(graphicsDevice, typeof(short), _indices.Count, BufferUsage.WriteOnly);
                _indexBuffer.SetData(_indices.ToArray());

                _isDirty = false;
            }

            graphicsDevice.SetVertexBuffer(_vertexBuffer);
            graphicsDevice.Indices = _indexBuffer;
        }

        public void DrawTo(GraphicsDevice graphicsDevice)
        {
            if (_vertices.Count == 0)
                return;

            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, _indexBuffer.IndexCount / 3);
        }
    }
}