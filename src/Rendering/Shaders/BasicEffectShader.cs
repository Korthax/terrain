using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Terrain.App.Rendering.Shaders
{
    public class BasicEffectShader : IEffectShader
    {
        private readonly BasicEffect _effect;

        public BasicEffectShader(BasicEffect effect)
        {
            _effect = effect;
        }

        public void SetCamera(Matrix viewMatrix, Matrix projectionMatrix)
        {
            _effect.View = viewMatrix;
            _effect.Projection = projectionMatrix;
        }
    }
}