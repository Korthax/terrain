using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Terrain.App.Rendering.Shaders
{
    public interface IEffectShader
    {
        void SetCamera(Matrix viewMatrix, Matrix projectionMatrix);
    }

    public class EffectShader : IEffectShader
    {
        private readonly Texture2D _texture;
        private readonly Effect _effect;

        public EffectShader(Effect effect, Texture2D texture)
        {
            _effect = effect;
            _texture = texture;
        }

        public void SetCamera(Matrix viewMatrix, Matrix projectionMatrix)
        {
            _effect.Parameters["xViewProjection"].SetValue(viewMatrix * projectionMatrix);
        }

        public void ApplyTo(IBuffer buffer, GraphicsDevice graphicsDevice)
        {
            _effect.CurrentTechnique = _effect.Techniques["HardwareInstancing"];
            _effect.Parameters["cubeTexture"].SetValue(_texture);
            foreach (var pass in _effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                buffer.DrawTo(graphicsDevice);
            }
        }
    }
}