using System.Linq;
using Microsoft.Xna.Framework;
using Terrain.App.Camera;
using Terrain.App.Map;
using Terrain.App.Types;
using Matrix = Microsoft.Xna.Framework.Matrix;

namespace Terrain.App.Rendering.Objects
{
    public class RenderObject
    {
        public InstanceData InstanceData { get; private set; }
        public BoundingBox Bounds { get; }
        public Vertex[] Vertices { get; }
        public Size3D Size { get; }

        public static RenderObject From(Vertex[] vertices, Matrix world, Vector2 atlasCoordinate)
        {
            var minX = float.MaxValue;
            var maxX = float.MinValue;

            var minY = float.MaxValue;
            var maxY = float.MinValue;

            var minZ = float.MaxValue;
            var maxZ = float.MinValue;

            foreach (var vertex in vertices)
            {
                if (vertex.Position.X < minX)
                    minX = vertex.Position.X;

                if (maxX < vertex.Position.X)
                    maxX = vertex.Position.X;

                if (vertex.Position.Y < minY)
                    minY = vertex.Position.Y;

                if (maxY < vertex.Position.Y)
                    maxY = vertex.Position.Y;

                if (vertex.Position.Z < minZ)
                    minZ = vertex.Position.Z;

                if (maxZ < vertex.Position.Z)
                    maxZ = vertex.Position.Z;
            }

            var boundingBox = BoundingBox.CreateFromPoints(vertices.Select(x => x.Position));
            return new RenderObject(vertices, boundingBox, new InstanceData(world, atlasCoordinate, ShaderFlag.None), new Size3D(maxX - minX, maxY - minY, maxZ - minZ));
        }

        private RenderObject(Vertex[] vertices, BoundingBox boundingBox, InstanceData instanceData, Size3D size)
        {
            Size = size;
            Bounds = boundingBox;
            InstanceData = instanceData;
            Vertices = vertices;
        }

        public bool Intersects(BoundingBox boundingBox)
        {
            return Bounds.Intersects(boundingBox);
        }

        public float? Intersects(Ray ray)
        {
            if(Bounds.Intersects(ray) == null)
                return null;

            float? closestIntersection = null;
            for (var i = 0; i < Vertices.Length; i += 3)
            {
                RayIntersectsTriangle(ref ray, Vertices[i].Position, Vertices[i + 1].Position, Vertices[i + 2].Position, out var intersection);

                if(intersection == null || (closestIntersection != null && !(intersection < closestIntersection)))
                    continue;

                closestIntersection = intersection;
            }

            return closestIntersection;
        }

        public bool IsVisible(ICamera camera)
        {
            return camera.IsVisible(Bounds);
        }

        public void SetFlag(ShaderFlag highlighted)
        {
            InstanceData = new InstanceData(InstanceData.World, InstanceData.AtlasCoordinate, highlighted);
        }

        private static void RayIntersectsTriangle(ref Ray ray, Vector3 vertex1, Vector3 vertex2, Vector3 vertex3, out float? result)
        {
            Vector3.Subtract(ref vertex2, ref vertex1, out var edge1);
            Vector3.Subtract(ref vertex3, ref vertex1, out var edge2);
            Vector3.Cross(ref ray.Direction, ref edge2, out var directionCrossEdge2);
            Vector3.Dot(ref edge1, ref directionCrossEdge2, out var determinant);

            if (determinant > -float.Epsilon && determinant < float.Epsilon)
            {
                result = null;
                return;
            }

            var inverseDeterminant = 1.0f / determinant;

            Vector3.Subtract(ref ray.Position, ref vertex1, out var distanceVector);
            Vector3.Dot(ref distanceVector, ref directionCrossEdge2, out var triangleU);
            triangleU *= inverseDeterminant;

            if (triangleU < 0 || triangleU > 1)
            {
                result = null;
                return;
            }

            Vector3.Cross(ref distanceVector, ref edge1, out var distanceCrossEdge1);
            Vector3.Dot(ref ray.Direction, ref distanceCrossEdge1, out var triangleV);
            triangleV *= inverseDeterminant;

            if (triangleV < 0 || triangleU + triangleV > 1)
            {
                result = null;
                return;
            }

            Vector3.Dot(ref edge2, ref distanceCrossEdge1, out var rayDistance);
            rayDistance *= inverseDeterminant;

            if (rayDistance < 0)
            {
                result = null;
                return;
            }

            result = rayDistance;
        }
    }
}