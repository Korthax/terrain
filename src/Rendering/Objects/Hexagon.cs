using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Terrain.App.Types;

namespace Terrain.App.Rendering.Objects
{
    public static class Hexagon
    {
        public static RenderObject Generate(Position3D origin, float radius, Vector2 atlasCoordinates)
        {
            var translation = Matrix.CreateTranslation(new Vector3(origin.X, 0, origin.Z));
            var scale = Matrix.Identity;

            if (origin.Y > 0f)
                scale = Matrix.CreateScale(1, origin.Y, 1);

            var world = scale * translation;

            var centreBottom = new Vector3(0, 0, 0);
            var centreTop = new Vector3(0, 5, 0);
            var result = new List<Vertex>();

            var cornersBottom = new List<Vector3>();
            var cornersTop = new List<Vector3>();
            for (var i = 0; i < 7; i++)
            {
                var angleDeg = 60 * i + 30;
                var angleRad = Math.PI / 180 * angleDeg;
                cornersBottom.Add(new Vector3(centreBottom.X + radius * (float)Math.Cos(angleRad), 0.0f, centreBottom.Z + radius * (float)Math.Sin(angleRad)));
                cornersTop.Add(new Vector3(centreTop.X + radius * (float)Math.Cos(angleRad), 5.0f, centreTop.Z + radius * (float)Math.Sin(angleRad)));
            }
            
            Vector2 Coords2(Vector3 vertex)
            {
                var x = (vertex.X + radius) / (radius * 2) * 0.75f;
                var y = (vertex.Z + radius) / (radius * 2);
                return new Vector2(x, y);
            }

            for (var i = 0; i < cornersBottom.Count; i++)
            {
                // Bottom
                result.Add(new Vertex(Vector3.Transform(centreBottom, world), Coords2(centreBottom), Vector2.Zero, Color.Green));
                result.Add(new Vertex(Vector3.Transform(cornersBottom[i + 1 < cornersBottom.Count ? i + 1 : 0], world), Coords2(cornersBottom[i + 1 < cornersBottom.Count ? i + 1 : 0]), Vector2.One, Color.Green));
                result.Add(new Vertex(Vector3.Transform(cornersBottom[i], world), Coords2(cornersBottom[i]), Vector2.One, Color.Green));

                // Top
                result.Add(new Vertex(Vector3.Transform(cornersTop[i], world), Coords2(cornersTop[i]), Vector2.One, Color.Red));
                result.Add(new Vertex(Vector3.Transform(cornersTop[i + 1 < cornersTop.Count ? i + 1 : 0], world), Coords2(cornersTop[i + 1 < cornersTop.Count ? i + 1 : 0]), Vector2.One, Color.Red));
                result.Add(new Vertex(Vector3.Transform(centreTop, world), Coords2(centreTop), Vector2.Zero, Color.Red));

                // Side Bottom
                result.Add(new Vertex(Vector3.Transform(cornersBottom[i + 1 < cornersBottom.Count ? i + 1 : 0], world), new Vector2(1, 1), Vector2.Zero, Color.Red));
                result.Add(new Vertex(Vector3.Transform(cornersTop[i], world), new Vector2(0.75f, 0), Vector2.Zero, Color.Red));
                result.Add(new Vertex(Vector3.Transform(cornersBottom[i], world), new Vector2(0.75f, 1), Vector2.Zero, Color.Red));

                // Side Top
                result.Add(new Vertex(Vector3.Transform(cornersBottom[i + 1 < cornersBottom.Count ? i + 1 : 0], world), new Vector2(1,1), Vector2.Zero, Color.Red));
                result.Add(new Vertex(Vector3.Transform(cornersTop[i + 1 < cornersTop.Count ? i + 1 : 0], world), new Vector2(1, 0), Vector2.Zero, Color.Red));
                result.Add(new Vertex(Vector3.Transform(cornersTop[i], world), new Vector2(0.75f, 0), Vector2.Zero, Color.Red));

            }

            return RenderObject.From(result.ToArray(), world, atlasCoordinates);
        }
    }
}