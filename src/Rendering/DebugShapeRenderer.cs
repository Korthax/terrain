using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Camera;
using Terrain.App.Rendering.Shaders;

namespace Terrain.App.Rendering
{
    public static class DebugShapeRenderer
    {
        public static bool Enabled = false;

        private class DebugShape
        {
            public VertexPositionColor[] Vertices;
            public float Lifetime;
            public int LineCount;
        }

        private const int SphereLineCount = (SphereResolution + 1) * 3;
        private const int SphereResolution = 30;

        private static readonly List<DebugShape> CachedShapes = new List<DebugShape>();
        private static readonly List<DebugShape> ActiveShapes = new List<DebugShape>();
        private static readonly Vector3[] Corners = new Vector3[8];

        private static VertexPositionColor[] _vertices = new VertexPositionColor[64];
        private static RasterizerState _graphicsDeviceRasterizerState;
        private static GraphicsDevice _graphics;
        private static Vector3[] _unitSphere;
        private static BasicEffect _effect;
        private static ICamera _camera;

        public static void Initialize(GraphicsDevice graphicsDevice, ICamera camera)
        {
            if (_graphics != null)
                throw new InvalidOperationException("Initialize can only be called once.");

            _graphics = graphicsDevice;
            _camera = camera;
            _effect = new BasicEffect(graphicsDevice)
            {
                VertexColorEnabled = true,
                TextureEnabled = false,
                DiffuseColor = Vector3.One,
                World = Matrix.Identity,
            };

            _graphicsDeviceRasterizerState = new RasterizerState
            {
                CullMode = CullMode.None,
                DepthClipEnable = true,
                FillMode = FillMode.WireFrame
            };
            InitializeSphere();
        }

        public static void AddLine(Vector3 a, Vector3 b, Color color, float lifespan = 0f)
        {
            var shape = GetShapeForLines(1, lifespan);
            shape.Vertices[0] = new VertexPositionColor(a, color);
            shape.Vertices[1] = new VertexPositionColor(b, color);
        }

        public static void AddTriangle(Vector3 a, Vector3 b, Vector3 c, Color color, float lifespan = 0f)
        {
            var shape = GetShapeForLines(3, lifespan);
            shape.Vertices[0] = new VertexPositionColor(a, color);
            shape.Vertices[1] = new VertexPositionColor(b, color);
            shape.Vertices[2] = new VertexPositionColor(b, color);
            shape.Vertices[3] = new VertexPositionColor(c, color);
            shape.Vertices[4] = new VertexPositionColor(c, color);
            shape.Vertices[5] = new VertexPositionColor(a, color);
        }

        public static void AddBoundingFrustum(BoundingFrustum frustum, Color color, float lifespan = 0f)
        {
            var shape = GetShapeForLines(12, lifespan);

            frustum.GetCorners(Corners);

            shape.Vertices[0] = new VertexPositionColor(Corners[0], color);
            shape.Vertices[1] = new VertexPositionColor(Corners[1], color);
            shape.Vertices[2] = new VertexPositionColor(Corners[1], color);
            shape.Vertices[3] = new VertexPositionColor(Corners[2], color);
            shape.Vertices[4] = new VertexPositionColor(Corners[2], color);
            shape.Vertices[5] = new VertexPositionColor(Corners[3], color);
            shape.Vertices[6] = new VertexPositionColor(Corners[3], color);
            shape.Vertices[7] = new VertexPositionColor(Corners[0], color);

            shape.Vertices[8] = new VertexPositionColor(Corners[4], color);
            shape.Vertices[9] = new VertexPositionColor(Corners[5], color);
            shape.Vertices[10] = new VertexPositionColor(Corners[5], color);
            shape.Vertices[11] = new VertexPositionColor(Corners[6], color);
            shape.Vertices[12] = new VertexPositionColor(Corners[6], color);
            shape.Vertices[13] = new VertexPositionColor(Corners[7], color);
            shape.Vertices[14] = new VertexPositionColor(Corners[7], color);
            shape.Vertices[15] = new VertexPositionColor(Corners[4], color);

            shape.Vertices[16] = new VertexPositionColor(Corners[0], color);
            shape.Vertices[17] = new VertexPositionColor(Corners[4], color);
            shape.Vertices[18] = new VertexPositionColor(Corners[1], color);
            shape.Vertices[19] = new VertexPositionColor(Corners[5], color);
            shape.Vertices[20] = new VertexPositionColor(Corners[2], color);
            shape.Vertices[21] = new VertexPositionColor(Corners[6], color);
            shape.Vertices[22] = new VertexPositionColor(Corners[3], color);
            shape.Vertices[23] = new VertexPositionColor(Corners[7], color);
        }

        public static void AddBoundingBox(BoundingBox box, Color color, float lifetime = 0f)
        {
            var shape = GetShapeForLines(12, lifetime);

            box.GetCorners(Corners);

            shape.Vertices[0] = new VertexPositionColor(Corners[0], color);
            shape.Vertices[1] = new VertexPositionColor(Corners[1], color);
            shape.Vertices[2] = new VertexPositionColor(Corners[1], color);
            shape.Vertices[3] = new VertexPositionColor(Corners[2], color);
            shape.Vertices[4] = new VertexPositionColor(Corners[2], color);
            shape.Vertices[5] = new VertexPositionColor(Corners[3], color);
            shape.Vertices[6] = new VertexPositionColor(Corners[3], color);
            shape.Vertices[7] = new VertexPositionColor(Corners[0], color);

            shape.Vertices[8] = new VertexPositionColor(Corners[4], color);
            shape.Vertices[9] = new VertexPositionColor(Corners[5], color);
            shape.Vertices[10] = new VertexPositionColor(Corners[5], color);
            shape.Vertices[11] = new VertexPositionColor(Corners[6], color);
            shape.Vertices[12] = new VertexPositionColor(Corners[6], color);
            shape.Vertices[13] = new VertexPositionColor(Corners[7], color);
            shape.Vertices[14] = new VertexPositionColor(Corners[7], color);
            shape.Vertices[15] = new VertexPositionColor(Corners[4], color);

            shape.Vertices[16] = new VertexPositionColor(Corners[0], color);
            shape.Vertices[17] = new VertexPositionColor(Corners[4], color);
            shape.Vertices[18] = new VertexPositionColor(Corners[1], color);
            shape.Vertices[19] = new VertexPositionColor(Corners[5], color);
            shape.Vertices[20] = new VertexPositionColor(Corners[2], color);
            shape.Vertices[21] = new VertexPositionColor(Corners[6], color);
            shape.Vertices[22] = new VertexPositionColor(Corners[3], color);
            shape.Vertices[23] = new VertexPositionColor(Corners[7], color);
        }

        public static void AddBoundingSphere(BoundingSphere sphere, Color color, float life = 0f)
        {
            var shape = GetShapeForLines(SphereLineCount, life);

            for (var i = 0; i < _unitSphere.Length; i++)
            {
                var vertexPosition = _unitSphere[i] * sphere.Radius + sphere.Center;
                shape.Vertices[i] = new VertexPositionColor(vertexPosition, color);
            }
        }

        public static void Render(GameTime gameTime)
        {
            if(!Enabled)
                return;

            _camera.SetOn(new BasicEffectShader(_effect));
            _graphics.RasterizerState = _graphicsDeviceRasterizerState;

            var vertexCount = ActiveShapes.Sum(shape => shape.LineCount * 2);
            if (vertexCount > 0)
            {
                if (_vertices.Length < vertexCount)
                    _vertices = new VertexPositionColor[vertexCount * 2];

                var lineCount = 0;
                var vertIndex = 0;
                foreach (var shape in ActiveShapes)
                {
                    lineCount += shape.LineCount;
                    var shapeVerts = shape.LineCount * 2;
                    for (var i = 0; i < shapeVerts; i++)
                        _vertices[vertIndex++] = shape.Vertices[i];
                }

                _effect.CurrentTechnique.Passes[0].Apply();

                var vertexOffset = 0;
                while (lineCount > 0)
                {
                    var linesToDraw = Math.Min(lineCount, 65535);
                    _graphics.DrawUserPrimitives(PrimitiveType.LineList, _vertices, vertexOffset, linesToDraw);
                    vertexOffset += linesToDraw * 2;
                    lineCount -= linesToDraw;
                }
            }

            var resort = false;
            for (var i = ActiveShapes.Count - 1; i >= 0; i--)
            {
                var activeShape = ActiveShapes[i];
                activeShape.Lifetime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(!(activeShape.Lifetime <= 0))
                    continue;

                CachedShapes.Add(activeShape);
                ActiveShapes.RemoveAt(i);
                resort = true;
            }
            
            if (resort)
                CachedShapes.Sort((shape1, shape2) => shape1.Vertices.Length.CompareTo(shape2.Vertices.Length));
        }

        private static void InitializeSphere()
        {
            _unitSphere = new Vector3[SphereLineCount * 2];

            const float step = MathHelper.TwoPi / SphereResolution;

            var index = 0;
            for (var a = 0f; a < MathHelper.TwoPi; a += step)
            {
                _unitSphere[index++] = new Vector3((float)Math.Cos(a), (float)Math.Sin(a), 0f);
                _unitSphere[index++] = new Vector3((float)Math.Cos(a + step), (float)Math.Sin(a + step), 0f);
            }

            for (var a = 0f; a < MathHelper.TwoPi; a += step)
            {
                _unitSphere[index++] = new Vector3((float)Math.Cos(a), 0f, (float)Math.Sin(a));
                _unitSphere[index++] = new Vector3((float)Math.Cos(a + step), 0f, (float)Math.Sin(a + step));
            }

            for (var a = 0f; a < MathHelper.TwoPi; a += step)
            {
                _unitSphere[index++] = new Vector3(0f, (float)Math.Cos(a), (float)Math.Sin(a));
                _unitSphere[index++] = new Vector3(0f, (float)Math.Cos(a + step), (float)Math.Sin(a + step));
            }
        }

        private static DebugShape GetShapeForLines(int lineCount, float lifetime)
        {
            DebugShape shape = null;

            var vertexCount = lineCount * 2;
            for (var i = 0; i < CachedShapes.Count; i++)
            {
                if(CachedShapes[i].Vertices.Length < vertexCount)
                    continue;

                shape = CachedShapes[i];
                CachedShapes.RemoveAt(i);
                ActiveShapes.Add(shape);
                break;
            }

            if (shape == null)
            {
                shape = new DebugShape { Vertices = new VertexPositionColor[vertexCount] };
                ActiveShapes.Add(shape);
            }

            shape.LineCount = lineCount;
            shape.Lifetime = lifetime;

            return shape;
        }
    }
}