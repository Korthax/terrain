﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Camera;
using Terrain.App.Input;
using Terrain.App.Map;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Buffers;
using Terrain.App.Rendering.Objects;
using Terrain.App.Rendering.Shaders;
using Terrain.App.Types;

namespace Terrain.App
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private InputDetector _inputDetector;
        private Renderer _renderer;
        private WorldMap _hexGrid;
        private IBuffer _buffer;
        private ICamera _camera;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;

            var hexagonRadius = 16;
            var mapSize = 64;

            var renderObject = Hexagon.Generate(Position3D.Zero, hexagonRadius, Vector2.Zero);
            var tileSize = new Size(renderObject.Size.Width, renderObject.Size.Depth);

            var noise = NoiseGenerator.GenerateNoise(new IndexSize(mapSize, mapSize), new MapOptions());
            _buffer = InstancedBuffer.From(GraphicsDevice, renderObject);
            _hexGrid = WorldMap.Generate(mapSize, renderObject.Size, hexagonRadius, noise);
            _hexGrid.AddTo(_buffer);

            _inputDetector = new InputDetector();
            _camera = new CompositeCamera(new StaticCamera(new Vector3(mapSize / 2f * tileSize.Width, 300, mapSize / 2f * tileSize.Width), GraphicsDevice), new FreeCamera(new Vector3(mapSize / 2f * tileSize.Width, 50, -100), GraphicsDevice));
            _camera.BindTo(_inputDetector);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _renderer = new Renderer(new EffectShader(Content.Load<Effect>("Shaders/Basic"), Content.Load<Texture2D>("Map/Tiles/Hex/HexAtlas")), GraphicsDevice, _camera);
            DebugShapeRenderer.Initialize(GraphicsDevice, _camera);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            var mousePosition = _inputDetector.MousePosition();
            var nearSource = new Vector3(mousePosition.X, mousePosition.Y, 0f);
            var farSource = new Vector3(mousePosition.X, mousePosition.Y, 1f);

            var nearPoint = _camera.Unproject(nearSource);
            var farPoint = _camera.Unproject(farSource);

            var direction = farPoint - nearPoint;
            direction.Normalize();

            _hexGrid.Pick(new Ray(nearPoint, direction));
            
            _inputDetector.Update();
            _camera.Update(gameTime, _inputDetector);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            _renderer.DrawFrom(_buffer, Matrix.Identity);
            _hexGrid.Render();
            DebugShapeRenderer.Render(gameTime);
            base.Draw(gameTime);
        }
    }
}