﻿using Microsoft.Xna.Framework;

namespace Terrain.App.Collisions
{
    public interface IIntersectable<T>
    {
        bool Intersects(BoundingBox boundingBox);
        IntersectResult<T> Intersects(Ray ray);
    }
}