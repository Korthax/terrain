﻿namespace Terrain.App.Collisions
{
    public class IntersectResult<T>
    {
        public static IntersectResult<T> None = new IntersectResult<T>(int.MaxValue, false, default(T));
        public bool HasOccured { get; }
        public float Distance { get; }
        public T Item { get; }


        public IntersectResult(float distance, bool hasOccured, T item)
        {
            Distance = distance;
            HasOccured = hasOccured;
            Item = item;
        }
    }
}