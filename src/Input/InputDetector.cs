﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using Microsoft.Xna.Framework.Input;
using Terrain.App.Types;

namespace Terrain.App.Input
{
    public interface IInputDetector
    {
        IObservable<InputEvent> OnKeysPressed { get; }
        IObservable<InputEvent> OnKeysDown { get; }
        IObservable<InputEvent> OnMouseMoved { get; }

        void Update();
    }

    public class InputEvent
    {
        public Position MousePosition { get; set; }
        public ModifierKeys Modifiers { get; set; }
        public HashSet<Keys> KeySet { get; set; }

        public static InputEvent From(HashSet<Keys> keys, Position mousePosition)
        {
            var modifiers = ModifierKeys.None;

            if(keys.Contains(Keys.LeftAlt))
                modifiers |= ModifierKeys.LeftAlt;

            if(keys.Contains(Keys.RightAlt))
                modifiers |= ModifierKeys.RightAlt;

            if(keys.Contains(Keys.LeftShift))
                modifiers |= ModifierKeys.LeftShift;

            if(keys.Contains(Keys.RightShift))
                modifiers |= ModifierKeys.RightShift;

            if (keys.Contains(Keys.LeftControl))
                modifiers |= ModifierKeys.LeftControl;

            if (keys.Contains(Keys.RightControl))
                modifiers |= ModifierKeys.RightControl;

            return new InputEvent
            {
                MousePosition = mousePosition,
                Modifiers = modifiers,
                KeySet = keys
            };
        }
    }

    [Flags]
    public enum ModifierKeys
    {
        None = 0,
        LeftAlt = 1,
        RightAlt = 2,
        LeftShift = 4,
        RightShift = 8,
        LeftControl = 16,
        RightControl = 32,
    }

    public class InputDetector : IInputDetector
    {
        public IObservable<InputEvent> OnKeysPressed => _onKeysPressed;
        public IObservable<InputEvent> OnKeysDown => _onKeysDown;
        public IObservable<InputEvent> OnMouseMoved => _onMouseMoved;

        private readonly Subject<InputEvent> _onKeysPressed;
        private readonly Subject<InputEvent> _onMouseMoved;
        private readonly Subject<InputEvent> _onKeysDown;

        private KeyboardState _oldKeyboardState;
        private MouseState _oldMouseState;

        public InputDetector()
        {
            _oldKeyboardState = Keyboard.GetState();
            _onMouseMoved = new Subject<InputEvent>();
            _onKeysPressed = new Subject<InputEvent>();
            _onKeysDown = new Subject<InputEvent>();
        }

        public void Update()
        {
            var newKeyboardState = Keyboard.GetState();
            var newMouseState = Mouse.GetState();

            var downKeys = new HashSet<Keys>(newKeyboardState.GetPressedKeys());
            var oldDownKeys = _oldKeyboardState.GetPressedKeys().ToList();
            var pressedKeys = new HashSet<Keys>(oldDownKeys.Where(x => !downKeys.Contains(x)));

            if (downKeys.Count > 0)
                _onKeysDown.OnNext(InputEvent.From(downKeys, new Position(newMouseState.X, newMouseState.Y)));

            if (pressedKeys.Count > 0)
                _onKeysPressed.OnNext(InputEvent.From(pressedKeys, new Position(newMouseState.X, newMouseState.Y)));

            _oldKeyboardState = newKeyboardState;
            _oldMouseState = newMouseState;
        }

        public Position MousePosition()
        {
            return new Position(_oldMouseState.X, _oldMouseState.Y);
        }

        public void SetMousePosition(int x, int y)
        {
            Mouse.SetPosition(x, y);
        }
    }
}