using System;
using Microsoft.Xna.Framework;
using Terrain.App.Map.Partitioning;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Objects;
using Terrain.App.Types;

namespace Terrain.App.Map
{
    public class WorldMap
    {
        private readonly OctTree<HexTile> _octTree;
        private readonly HexTile[][] _grid;

        private HexTile _currentTile;

        public static WorldMap Generate(int maxSize, Size3D tileSize, int radius, Islands noiseMap)
        {
            var halfSize = tileSize / 2;
            var boundsX = -halfSize.Width;
            var boundsY = -halfSize.Depth;

            DebugShapeRenderer.Enabled = true;
            var octTree = OctTree<HexTile>.From(new Position3D(boundsX, 0, boundsY), tileSize * maxSize, 0, tileSize);

            var tiles = new HexTile[maxSize][];
            for (var row = 0; row < maxSize; row++)
            {
                tiles[row] = new HexTile[maxSize];
                for (var col = 0; col < maxSize; col++)
                {
                    var index = new Index(col, row);

                    var x = halfSize.Depth * (float)Math.Sqrt(3) * (col - 0.5f * (row & 1));
                    var y = halfSize.Depth * (3 / 2f) * row;
                    var node = noiseMap[col, row];

                    var hexTile = new HexTile(index, TileType.Edge, Hexagon.Generate(new Position3D(x, node.Elevation, y), halfSize.Depth, node.Type.ToTextureCoords()));

                    octTree.Add(hexTile);
                    tiles[row][col] = hexTile;
                }
            }

            return new WorldMap(tiles, octTree);
        }

        private WorldMap(HexTile[][] grid, OctTree<HexTile> octTree)
        {
            _grid = grid;
            _octTree = octTree;
        }

        public void AddTo(IBuffer buffer)
        {
            foreach (var row in _grid)
            {
                foreach (var tile in row)
                {
                    tile.AddTo(buffer);
                }
            }
        }

        public void Pick(Ray ray)
        {
            var intersections = _octTree.Pick(ray);
            if(!intersections.HasOccured)
                return;

            if(_currentTile != null && _currentTile != intersections.Item)
                _currentTile.SetFlag(ShaderFlag.None);

            _currentTile = intersections.Item;
            _currentTile.SetFlag(ShaderFlag.Highlighted);
        }

        public void Render()
        {
            _octTree.Render();
        }
    }
}