﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Terrain.App.Collisions;
using Terrain.App.Rendering;
using Terrain.App.Types;

namespace Terrain.App.Map.Partitioning
{
    public class OctTree<T> where T : class, IIntersectable<T>
    {
        private readonly Position3D _position;
        private readonly BoundingBox _bounds;
        private readonly List<T> _objects;
        private readonly Size3D _minSize;
        private readonly Size3D _size;
        private readonly int _level;

        private OctTree<T>[] _children;

        public static OctTree<T> From(Position3D position, Size3D size, int level, Size3D minSize)
        {
            var vector3 = new Vector3(position.X, position.Y, position.Z);
            var polygon = new BoundingBox(vector3, vector3 + new Vector3(size.Width, size.Height, size.Depth));
            //DebugShapeRenderer.AddBoundingBox(polygon, Color.Purple, 50);
            return new OctTree<T>(position, size, null, level, polygon, new List<T>(), minSize);
        }

        private OctTree(Position3D position, Size3D size, OctTree<T>[] children, int level, BoundingBox bounds, List<T> rectangles, Size3D minSize)
        {
            _position = position;
            _size = size;
            _children = children;
            _level = level;
            _bounds = bounds;
            _objects = rectangles;
            _minSize = minSize;
        }

        public void Add(T polygon)
        {
            var collisionResponse = polygon.Intersects(_bounds);
            if (!collisionResponse)
                return;

            var newSize = _size / 2;
            if (newSize.Volume <= _minSize.Volume)
            {
                _objects.Add(polygon);
                return;
            }

            if (_children == null)
            {
                _children = new OctTree<T>[8];
                _children[0] = From(new Position3D(_position.X, _position.Y, _position.Z), newSize, _level + 1, _minSize);
                _children[1] = From(new Position3D(_position.X + newSize.Width, _position.Y, _position.Z), newSize, _level + 1, _minSize);
                _children[2] = From(new Position3D(_position.X, _position.Y, _position.Z + newSize.Depth), newSize, _level + 1, _minSize);
                _children[3] = From(new Position3D(_position.X + newSize.Width, _position.Y, _position.Z + newSize.Depth), newSize, _level + 1, _minSize);

                _children[4] = From(new Position3D(_position.X, _position.Y + newSize.Height, _position.Z), newSize, _level + 1, _minSize);
                _children[5] = From(new Position3D(_position.X + newSize.Width, _position.Y + newSize.Height, _position.Z), newSize, _level + 1, _minSize);
                _children[6] = From(new Position3D(_position.X, _position.Y + newSize.Height, _position.Z + newSize.Depth), newSize, _level + 1, _minSize);
                _children[7] = From(new Position3D(_position.X + newSize.Width, _position.Y + newSize.Height, _position.Z + newSize.Depth), newSize, _level + 1, _minSize);
            }

            foreach (var child in _children)
                child?.Add(polygon);
        }

        public IntersectResult<T> Pick(Ray ray)
        {
            if(!ray.Intersects(_bounds).HasValue)
                return IntersectResult<T>.None;

            var closestIntersection = IntersectResult<T>.None;
            if (_children == null)
            {
                foreach(var obj in _objects)
                {
                    var intersectResult = obj.Intersects(ray);
                    if(!intersectResult.HasOccured || closestIntersection.Distance < intersectResult.Distance)
                        continue;

                    closestIntersection = intersectResult;
                }

                return closestIntersection;
            }

            foreach (var child in _children)
            {
                var intersectResult = child.Pick(ray);
                if (!intersectResult.HasOccured || closestIntersection.Distance < intersectResult.Distance)
                    continue;

                closestIntersection = intersectResult;
            }

            return closestIntersection;
        }

        public void Render()
        {
            if (_children == null)
                return;

            foreach (var child in _children)
                child?.Render();
        }
    }
}