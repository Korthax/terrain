﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Terrain.App.Types;

namespace Terrain.App.Map.Partitioning
{
    public class QuadTree
    {
        private const int MinSize = 5;

        private readonly List<BoundingBox> _objects;
        private readonly QuadTree[] _children;
        private readonly BoundingBox _polygon;
        private readonly Position _position;
        private readonly int _level;
        private readonly int _size;

        public static QuadTree From(Position position, int size, int level)
        {
            var polygon = new BoundingBox(new Vector3(position.X, 0, position.Y), new Vector3(position.X + size, 1, position.Y + size));
            return new QuadTree(position, size, new QuadTree[4], level, polygon, new List<BoundingBox>());
        }

        private QuadTree(Position position, int size, QuadTree[] children, int level, BoundingBox polygon, List<BoundingBox> objects)
        {
            _position = position;
            _size = size;
            _children = children;
            _level = level;
            _polygon = polygon;
            _objects = objects;
        }

        public void Add(BoundingBox polygon)
        {
            var collisionResponse = polygon.Intersects(_polygon);
            if (!collisionResponse)
                return;

            _objects.Add(polygon);
            if (_size <= MinSize)
                return;

            if (_children.Length == 0)
            {
                var newSize = _size / 2;
                _children[0] = From(new Position(_position.X, _position.Y), newSize, _level + 1);
                _children[1] = From(new Position(_position.X + newSize, _position.Y), newSize, _level + 1);
                _children[2] = From(new Position(_position.X + newSize, _position.Y + newSize), newSize, _level + 1);
                _children[3] = From(new Position(_position.X, _position.Y + newSize), newSize, _level + 1);
            }

            foreach (var child in _children)
                child.Add(polygon);
        }

        public bool Contains(Position position)
        {
            return position.X >= _position.X
                   && position.Y >= _position.Y
                   && position.X < _position.X + _size
                   && position.Y < _position.Y + _size;
        }

        public void Render()
        {
            foreach (var child in _children)
                child.Render();
        }
    }
}