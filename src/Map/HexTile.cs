using Microsoft.Xna.Framework;
using Terrain.App.Collisions;
using Terrain.App.Rendering;
using Terrain.App.Rendering.Objects;
using Terrain.App.Types;

namespace Terrain.App.Map
{
    public enum TileType
    {
        Edge,
        Land
    }

    public class HexTile : IIntersectable<HexTile>
    {
        private readonly RenderObject _renderObject;
        private readonly TileType _tileType;
        private readonly Index _index;

        public HexTile(Index index, TileType tileType, RenderObject renderObject)
        {
            _index = index;
            _tileType = tileType;
            _renderObject = renderObject;
        }

        public void SetFlag(ShaderFlag shaderFlag)
        {
            _renderObject.SetFlag(shaderFlag);
        }

        public bool Intersects(BoundingBox boundingBox)
        {
            return _renderObject.Intersects(boundingBox);
        }

        public IntersectResult<HexTile> Intersects(Ray ray)
        {
            var intersects = _renderObject.Intersects(ray);
            return intersects == null ? IntersectResult<HexTile>.None : new IntersectResult<HexTile>(intersects.Value, true, this);
        }

        public RenderObject ToRenderObject()
        {
            return _renderObject;
        }

        public void AddTo(IBuffer buffer)
        {
          //  if(_tileType == TileType.Land)
                buffer.TryAdd(_renderObject);
        }
    }
}