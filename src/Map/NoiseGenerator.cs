﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terrain.App.Types;

namespace Terrain.App.Map
{
    public class NoiseOptions
    {
        public double Increments { get; set; } = 10d;
        public int Tiers { get; set; } = 4;
        public int MaxHeight { get; set; } = 20;
        public double A { get; set; } = 0.05;
        public double B { get; set; } = 1.00;
        public double C { get; set; } = 2.50;
    }

    public static class NoiseGenerator
    {
        public static Islands GenerateNoise(IndexSize size, MapOptions options)
        {
            var seed = Environment.TickCount;

            var elevations = MixNoise(size, new[] { 1, 1 / 4d, 1 / 8d, 1 / 16d }, 2, seed);
            var moisture = MixNoise(size, new[] { 1, 1 / 2d, 1 / 4d }, 2, 5551212 + seed);

            for (var y = 0; y < size.Height; y++)
            {
                for (var x = 0; x < size.Width; x++)
                {
                    var nx = x / (double)size.Width - 0.5;
                    var ny = y / (double)size.Height - 0.5;
                    var d = 2 * Math.Sqrt(nx * nx + ny * ny);

                    var baseElevation = elevations[x, y];
                    var elevation = Math.Round(baseElevation * options.Noise.Tiers) / options.Noise.Tiers;
                    elevation = (elevation + options.Noise.A) * (1 - options.Noise.B * Math.Pow(d, options.Noise.C));

                    if (elevation < 0.0)
                        elevation = 0.0;

                    elevation = (int)Math.Round(((int)(options.Noise.MaxHeight * elevation) | 0) / options.Noise.Increments) * options.Noise.Increments;

                    if (elevation < 10.0)
                        elevation = 0.0;

                    elevations[x, y] = (int)elevation;
                }
            }

            return Islands.From(elevations, moisture, options, seed + 99);
        }

        private static double[,] MixNoise(IndexSize size, double[] spectrum, double frequency, int seed)
        {
            var maps = new List<double[,]>();
            var amplitudes = new List<double>();

            var scale = 0.0;
            var exponent = 1;
            for (var octave = 0; octave < spectrum.Length; octave++, exponent *= 2)
            {
                scale += spectrum[octave];
                maps.Add(FillNoise(size, frequency * exponent, seed + octave));
                amplitudes.Add(spectrum[octave]);
            }

            return AddMaps(size, maps, amplitudes.Select(a => a / scale).ToList());
        }

        private static double[,] AddMaps(IndexSize size, List<double[,]> maps, List<double> amplitudes)
        {
            var mapsCount = maps.Count;
            if (mapsCount != amplitudes.Count)
                throw new Exception("maps and amplitudes must be same length");

            var output = new double[size.Width, size.Height];
            for (var y = 0; y < size.Height; y++)
            {
                for (var x = 0; x < size.Width; x++)
                {
                    var z = 0d;
                    for (var i = 0; i < mapsCount; i++)
                        z += amplitudes[i] * maps[i][x, y];

                    output[x, y] = z;
                }
            }

            return output;
        }

        private static double[,] FillNoise(IndexSize size, double frequency, int seed)
        {
            var map = new double[size.Width, size.Height];
            var noise = new SimplexNoise(new Random(seed));
            var aspect = size.Width / size.Height;
            for (var y = 0; y < size.Height; y++)
            {
                for (var x = 0; x < size.Width; x++)
                {
                    var nx = x / (float)size.Width - 0.5f;
                    var ny = y / (float)size.Height - 0.5f;
                    var z = noise.Noise2D(nx * frequency * aspect, ny * frequency);
                    map[x, y] = z / 2 + 0.5;
                }
            }

            return map;
        }
    }

    public class Islands
    {
        public Node this[int x, int y] => _nodes[x, y];

        private readonly Node[,] _nodes;

        public static Islands From(double[,] elevations, double[,] moisture, MapOptions options, int seed)
        {
            var random = new Random(seed);
            var width = elevations.GetUpperBound(0) + 1;
            var height = elevations.GetUpperBound(1) + 1;
            var size = new IndexSize(width, height);

            var nodes = new Node[width, height];
            var landMass = new List<Node>();
            var masses = new HashSet<Node>();
            for(var x = 0; x < width; x++)
            {
                for(var y = 0; y < height; y++)
                {
                    var type = NodeType.Ocean;
                    var current = new Index(x, y);
                    var sameHeight = false;
                    var heightSum = 0d;
                    var bordersOcean = false;
                    foreach (var neighbour in Index.AllNeighbours)
                    {
                        var next = current + neighbour;
                        if(next.X < 0 || next.X >= width || next.Y < 0 || next.Y >= height)
                            continue;

                        var nextHeight = elevations[next.X, next.Y];
                        bordersOcean = bordersOcean || nextHeight == 0;
                        sameHeight = sameHeight || nextHeight == elevations[x, y];
                        heightSum += nextHeight;
                    }

                    var elevation = elevations[x,y];
                    if (!sameHeight)
                        elevation = heightSum / Index.Neighbours.Length;

                    var node = new Node(current)
                    {
                        Elevation = (int)elevation,
                        Type = type,
                        Moisture = moisture[x, y],
                    };

                    if(Math.Abs(elevation) < float.Epsilon)
                    {
                        node.Elevation = 8;
                    }
                    else
                    {
                        if (bordersOcean)
                            node.Type = NodeType.Coast;
                        else if (elevation > 10)
                            node.Type = NodeType.Mountains;
                        else
                        {
                            node.Type = NodeType.Grassland;
                            landMass.Add(node);
                        }

                        masses.Add(node);
                    }

                    nodes[x, y] = node;
                }
            }

            var islands = new List<Island>();
            var processed = new HashSet<Node>();
            Island largestIsland = null;
            while (masses.Count > 0)
            {
                var currentNode = masses.First();
                processed.Add(currentNode);

                var islandNodes = new HashSet<Node> { currentNode };

                var fillList = new Queue<Node>();
                fillList.Enqueue(currentNode);

                while(fillList.Count > 0)
                {
                    var n = fillList.Dequeue();

                    if(masses.Contains(n))
                        masses.Remove(n);
                    else
                        continue;

                    foreach (var neighbour in n.GetNeighbours(size))
                    {
                        var neighbourNode = nodes[neighbour.X, neighbour.Y];
                        if (neighbourNode.Type == NodeType.Ocean)
                            continue;

                        islandNodes.Add(neighbourNode);
                        fillList.Enqueue(neighbourNode);
                    }
                }

                var island = Island.From(islandNodes);

                if(largestIsland == null)
                {
                    largestIsland = island;
                    islands.Add(island);
                }
                else if(largestIsland.Size() < island.Size())
                {
                    largestIsland = island;

                    islands = islands
                        .Where(oldIsland => !oldIsland.TryMergeInto(largestIsland, nodes, out largestIsland))
                        .ToList();

                    islands.Add(largestIsland);
                }
                else
                {
                    if(!island.TryMergeInto(largestIsland, nodes, out largestIsland))
                        islands.Add(island);
                }
            }

            var numberOfTiles = (int)(landMass.Count / 100d * options.ResourcePercentage);
            var tenPercent = (int)(numberOfTiles * 0.1);
            var pointsRemaining = numberOfTiles;

            var forestTiles = new List<int>();
            while (pointsRemaining > 0)
            {
                var pointsToTake = random.Next(0, tenPercent);
                pointsRemaining -= pointsToTake;

                int tileIndex;
                do
                {
                    tileIndex = random.Next(0, landMass.Count);
                } while (forestTiles.Contains(tileIndex));

                forestTiles.Add(tileIndex);

                var centre = landMass[tileIndex];
                centre.Type = NodeType.Dirt;
                Seed(centre, pointsToTake, width, height, nodes);
            }

            return new Islands(nodes);
        }

        private static void Seed(Node centre, int remaining, int width, int height, Node[,] nodes)
        {
            var k = 0;
            while (remaining > 0)
            {
                k++;

                var cube = centre.Index;
                for(var i = 0; i < k; i++)
                    cube += nodes[cube.X, cube.Y].GetDirection(4);

                for(var i = 0; i < 6; i++)
                {
                    for(var j = 0; j < k; j++)
                    {
                        if (cube.X >= width || cube.Y >= height || cube.X < 0 || cube.Y < 0)
                            continue;

                        var node = nodes[cube.X, cube.Y];
                        cube = node.GetNeightbour(i);

                        if(node.Type != NodeType.Grassland)
                            continue;

                        node.Type = NodeType.Forest;
                    }
                }

                remaining -= 6 * k;
            }
        }

        private Islands(Node[,] nodes)
        {
            _nodes = nodes;
        }

        public void SaveToFile(GraphicsDevice graphicsDevice, string fileName)
        {
            var colourLookup = new Color[256];
            for (var i = 0; i < 256; i++)
            {
                colourLookup[i] = new Color(
                    (int)(i * 0.6f + 0.4f * 255f),
                    (int)(i * 0.5f + 0.5f * 255f),
                    (int)(i * 0.6f + 0.4f * 255f)
                );
            }

            var width = _nodes.GetUpperBound(0) + 1;
            var height = _nodes.GetUpperBound(1) + 1;
            var colours = new Color[width * height];
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var index = y * width + x;
                    var z = _nodes[x, y];
                    colours[index] = z.Elevation == 0 || z.Elevation == 8
                        ? new Color(96, 128, 255, 255)
                        : colourLookup[z.Elevation];
                }
            }

            Directory.CreateDirectory(new FileInfo(fileName).DirectoryName);
            using (var stream = File.Create(fileName))
            using (var texture = new Texture2D(graphicsDevice, width, height))
            {
                texture.SetData(colours);
                texture.SaveAsPng(stream, width, height);
            }
        }
    }

    public class Island
    {
        private readonly HashSet<Node> _nodes;
        private readonly IndexSize _size;
        private readonly Node[,] _grid;
        private readonly Node _centre;

        public static Island From(HashSet<Node> nodes)
        {
            var minX = int.MaxValue;
            var minY = int.MaxValue;
            var maxX = int.MinValue;
            var maxY = int.MinValue;

            foreach(var node in nodes)
            {
                if(node.Index.X < minX)
                    minX = node.Index.X;

                if(node.Index.Y < minY)
                    minY= node.Index.Y;

                if(node.Index.X > maxX)
                    maxX = node.Index.X;

                if(node.Index.Y > maxY)
                    maxY = node.Index.Y;
            }

            var size = new IndexSize(maxX - minX, maxY - minY);
            var grid = new Node[size.Width + 1, size.Height + 1];

            foreach(var node in nodes)
                grid[node.Index.X - minX, node.Index.Y - minY] = node;

            var centre = grid[size.Width / 2, size.Height / 2];

            return new Island(nodes, grid, centre, size);
        }

        private Island(HashSet<Node> nodes, Node[,] grid, Node centre, IndexSize size)
        {
            _nodes = nodes;
            _grid = grid;
            _centre = centre;
            _size = size;
        }

        public int Size()
        {
            return _nodes.Count;
        }

        public bool TryMergeInto(Island largestIsland, Node[,] map, out Island island)
        {
            island = largestIsland;
            if(_nodes.Count < 10)
                return false;


            island = From(new HashSet<Node>(_nodes.Select(x => new Node(x.Index) {  }).Union(largestIsland._nodes)));
            return true;
        }
    }
    
    public class MapOptions
    {
        public NoiseOptions Noise { get; set; } = new NoiseOptions();
        public int ResourcePercentage { get; set; } = 30;
    }

    public class Node
    {
        private static readonly Index[][] Directions = {
            new[]
            {
                new Index(+1, 0), new Index(+1, -1), new Index(0, -1),
                new Index(-1, 0), new Index(0, +1), new Index(+1, +1)
            },
            new[]
            {
                new Index(+1, 0), new Index(0, -1), new Index(-1, -1),
                new Index(-1, 0), new Index(-1, +1), new Index(0, +1)
            }
        };

        public int Elevation { get; set; }
        public double Moisture { get; set; }
        public NodeType Type { get; set; }
        public Index Index { get; }

        public Node(Index index)
        {
            Index = index;
        }

        public IEnumerable<Index> GetNeighbours(IndexSize bounds)
        {
            return Enumerable
                .Range(0, 6)
                .Select(x => Index + GetDirection(x))
                .Where(bounds.Contains);
        }

        public Index GetNeightbour(int direction)
        {
            var neighbour = GetDirection(direction);
            return new Index(Index.X + neighbour.X, Index.Y + neighbour.Y);
        }

        public Index GetDirection(int direction)
        {
            var parity = Index.Y & 1;
            return Directions[parity][direction];
        }

        protected bool Equals(Node other)
        {
            return Equals(Index, other.Index);
        }

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(null, obj))
                return false;

            if(ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((Node)obj);
        }

        public override int GetHashCode()
        {
            return Index != null ? Index.GetHashCode() : 0;
        }
    }

    public enum NodeType
    {
        Ocean,
        Grassland,
        Coast,
        Mountains,
        Forest,
        Dirt
    }

    public static class NodeTypeExtensions
    {
        public static Vector2 ToTextureCoords(this NodeType nodeType)
        {
            switch (nodeType)
            {
                case NodeType.Ocean:
                    return new Vector2(1, 1);
                case NodeType.Grassland:
                case NodeType.Mountains:
                    return new Vector2(0, 1);
                case NodeType.Coast:
                    return new Vector2(1, 0);
                case NodeType.Forest:
                    return new Vector2(1, 2);
                case NodeType.Dirt:
                    return new Vector2(3, 0);
                default:
                    throw new ArgumentOutOfRangeException(nameof(nodeType), nodeType, null);
            }
        }
    }

    // Based on a fast javascript implementation of simplex noise by Jonas Wagner
    public class SimplexNoise
    {
        private readonly double F2 = 0.5 * (Math.Sqrt(3.0) - 1.0);
        private readonly double G2 = (3.0 - Math.Sqrt(3.0)) / 6.0;

        private readonly float[] _gradients =
        {
            1, 1, 0,
            -1, 1, 0,
            1, -1, 0,

            -1, -1, 0,
            1, 0, 1,
            -1, 0, 1,

            1, 0, -1,
            -1, 0, -1,
            0, 1, 1,

            0, -1, 1,
            0, 1, -1,
            0, -1, -1
        };

        private readonly int[] _permutationMod12;
        private readonly int[] _permutations;

        public SimplexNoise(Random random)
        {
            _permutations = new int[512];
            _permutationMod12 = new int[512];

            var permutationTable = BuildPermutationTable(random);
            for (var i = 0; i < 512; i++)
            {
                _permutations[i] = permutationTable[i & 255];
                _permutationMod12[i] = _permutations[i] % 12;
            }
        }

        public double Noise2D(double xin, double yin)
        {
            double n0 = 0; // Noise contributions from the three corners
            double n1 = 0;
            double n2 = 0;

            // Skew the input space to determine which simplex cell we're in
            var s = (xin + yin) * F2; // Hairy factor for 2D
            var i = (int)Math.Floor(xin + s);
            var j = (int)Math.Floor(yin + s);
            var t = (i + j) * G2;
            var x0 = xin - (i - t); // // Unskew the cell origin back to the x,y distances from the cell origin
            var y0 = yin - (j - t);

            // Determine which simplex we are in.
            int i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if (x0 > y0) // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            {
                i1 = 1;
                j1 = 0;
            }
            else // upper triangle, YX order: (0,0)->(0,1)->(1,1)
            {
                i1 = 0;
                j1 = 1;
            }

            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6
            var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            var y1 = y0 - j1 + G2;
            var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
            var y2 = y0 - 1.0 + 2.0 * G2;

            // Work out the hashed gradient indices of the three simplex corners
            var ii = i & 255;
            var jj = j & 255;

            // Calculate the contribution from the three corners
            var t0 = 0.5 - x0 * x0 - y0 * y0;
            if (t0 >= 0)
            {
                var gi0 = _permutationMod12[ii + _permutations[jj]] * 3;
                t0 *= t0;
                n0 = t0 * t0 * (_gradients[gi0] * x0 + _gradients[gi0 + 1] * y0); // (x,y) of grad3 used for 2D gradient
            }

            var t1 = 0.5 - x1 * x1 - y1 * y1;
            if (t1 >= 0)
            {
                var gi1 = _permutationMod12[ii + i1 + _permutations[jj + j1]] * 3;
                t1 *= t1;
                n1 = t1 * t1 * (_gradients[gi1] * x1 + _gradients[gi1 + 1] * y1);
            }

            var t2 = 0.5 - x2 * x2 - y2 * y2;
            if (t2 >= 0)
            {
                var gi2 = _permutationMod12[ii + 1 + _permutations[jj + 1]] * 3;
                t2 *= t2;
                n2 = t2 * t2 * (_gradients[gi2] * x2 + _gradients[gi2 + 1] * y2);
            }

            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 70.0 * (n0 + n1 + n2);
        }

        private static int[] BuildPermutationTable(Random random)
        {
            var p = new int[256];
            for (var i = 0; i < 256; i++)
                p[i] = i;

            return p
                .OrderBy(r => random.Next())
                .ToArray();
        }
    }
}