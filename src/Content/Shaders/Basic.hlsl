﻿texture cubeTexture;
float4x4 xViewProjection;

sampler TextureSampler = sampler_state
{
    texture = <cubeTexture>;
    mipfilter = LINEAR;
    minfilter = LINEAR;
    magfilter = LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct InstancingVSinput
{
    float4 Position : POSITION0;
    float4 Color : COLOR0;
    float2 TexCoord : TEXCOORD0;
    float2 EdgeCoords : TEXCOORD1;
};

struct VertexToPixel
{
    float4 Position : POSITION;
    float4 Color : COLOR0;
    float2 TexCoords : TEXCOORD0;
    float2 EdgeCoords : TEXCOORD1;
    float Highlight : TEXCOORD2;
};

struct PixelToFrame
{
    float4 Color : COLOR0;
};

VertexToPixel HardwareInstancingVertexShader(InstancingVSinput input, float4x4 instanceTransform : BLENDWEIGHT, float2 atlasCoords : TEXCOORD2, int flags : TEXCOORD3)
{
    VertexToPixel Output = (VertexToPixel)0;

    float4x4 trans = transpose(instanceTransform);
    Output.Position =  mul(mul(input.Position, trans), xViewProjection);
    Output.Color = input.Color;
    Output.TexCoords = float2((input.TexCoord.x / 4.0f) + (1.0f / 4.0f * atlasCoords.x), (input.TexCoord.y / 4.0f) + (1.0f / 4.0f * atlasCoords.y));
    Output.EdgeCoords = input.EdgeCoords;
	Output.Highlight = flags;
    return Output;
}

VertexToPixel SimplestVertexShader( float4 inPos : POSITION, float4 inColor : COLOR0, float2 inTexCoords : TEXCOORD0)
{
    VertexToPixel Output = (VertexToPixel)0;
    
    Output.Position = mul(inPos, xViewProjection);


    Output.Color = inColor;
    Output.TexCoords = inTexCoords;
    Output.Highlight = false;

    return Output;
}

PixelToFrame OurFirstPixelShader(VertexToPixel PSIn)
{
    PixelToFrame Output = (PixelToFrame)0;

    if(PSIn.EdgeCoords.x > 0.95f)
    {
		if(PSIn.Highlight == 0.0)
		{
			Output.Color = float4(0, 0, 0, 1);
		}
		else
		{
			Output.Color = float4(1, 1, 1, 1);
		}
    }
    else
    {
        Output.Color = tex2D(TextureSampler, PSIn.TexCoords);
    }
    return Output;
}

technique Simplest
 {
     pass Pass0
     {
         VertexShader = compile vs_4_0_level_9_3 SimplestVertexShader();
         PixelShader = compile ps_4_0_level_9_3 OurFirstPixelShader();
     }
}

technique HardwareInstancing
{
    pass Pass1
    {
        VertexShader = compile vs_4_0_level_9_3 HardwareInstancingVertexShader();
        PixelShader = compile ps_4_0_level_9_3 OurFirstPixelShader();
    }
}